package com.x.wcrm.assemble.control.jaxrs.config;

import com.x.base.core.project.jaxrs.StandardJaxrsAction;
import com.x.base.core.project.logger.Logger;
import com.x.base.core.project.logger.LoggerFactory;
import com.x.wcrm.assemble.control.service.ConfigQueryService;
import com.x.wcrm.assemble.control.service.RecordService;

public class BaseAction extends StandardJaxrsAction {
	private final Logger logger = LoggerFactory.getLogger(BaseAction.class);
	protected ConfigQueryService configQueryService = new ConfigQueryService();
	RecordService recordService = new RecordService();

}
