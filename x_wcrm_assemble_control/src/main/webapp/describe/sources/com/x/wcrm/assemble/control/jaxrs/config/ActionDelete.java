package com.x.wcrm.assemble.control.jaxrs.config;

import com.x.base.core.container.EntityManagerContainer;
import com.x.base.core.container.factory.EntityManagerContainerFactory;
import com.x.base.core.entity.annotation.CheckRemoveType;
import com.x.base.core.project.exception.ExceptionEntityNotExist;
import com.x.base.core.project.http.ActionResult;
import com.x.base.core.project.http.EffectivePerson;
import com.x.base.core.project.jaxrs.WoId;
import com.x.wcrm.core.entity.WCrmConfig;

//删除配置（管理员，crm管理员可以删除）
public class ActionDelete extends BaseAction {

	ActionResult<Wo> execute(EffectivePerson effectivePerson, String id) throws Exception {
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			ActionResult<Wo> result = new ActionResult<>();
			WCrmConfig o = emc.find(id, WCrmConfig.class);
			if (null == o) {
				throw new ExceptionEntityNotExist(id, WCrmConfig.class);
			}
			emc.beginTransaction( WCrmConfig.class );
			emc.remove( o , CheckRemoveType.all );
			emc.commit();
			Wo wo = new Wo();
			result.setData(wo);
			return result;
		}
	}

	static class Wo extends WoId {

	}
}
